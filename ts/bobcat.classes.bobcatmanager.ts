import * as plugins from './bobcat.plugins';
import { Bobcat } from './bobcat.classes.bobcat';

/**
 * a manager for managing multiple bobcats
 */
export class BobcatManager {
  public taskmanager = new plugins.taskbuffer.TaskManager();
  public bobcats: Bobcat[] = [];

  /**
   * a store for knowing what has happened retrospectively
   */
  public actionStore: {
    actionName: string;
    actionPayload: string;
  }[] = [];

  constructor() {
    this.taskmanager.addAndScheduleTask(new plugins.taskbuffer.Task({
      name: 'contMaintenance',
      taskFunction: async () => {
        this.actionStore.push();
        await this.runMaintenance();
      }
    }), '0 0 * * * *');
    this.taskmanager.addAndScheduleTask(new plugins.taskbuffer.Task({
      name: 'contStatus',
      taskFunction: async () => {
        this.actionStore.push();
        for (const bobcat of this.bobcats) {
          bobcat.checkMinerStatus();
        };
      }
    }), '0 * * * * *');
  }

  /**
   * adds a bobcat to the manager
   * @param networkAddressArg
   */
  public async addBobcat (networkAddressArg: string) {
    const newBobcat = await Bobcat.createFromNetworkAddress(networkAddressArg);
    this.bobcats.push(newBobcat);
    console.log(`added ${newBobcat.latestMinerDetails?.animal} at ${newBobcat.networkAddress}`);
  }

  /**
   * runs the maintenance on all managed bobcats
   */
  public async runMaintenance () {
    console.log(`now running maintenance on ${this.bobcats.length} bobcats!`);
    console.log(`cooling down for 10 seconds`);
    await plugins.smartdelay.delayFor(10000);
    for (const bobcat of this.bobcats) {
      console.log(`now running maintenance on ${bobcat.latestMinerDetails?.animal} at ${bobcat.networkAddress}`);
      await plugins.smartpromise.timeoutAndContinue(bobcat.runMaintenance());
    }
  };

  /**
   * starts continuous maintenance of the bobcat miners
   */
  public async startTaskmanager () {
    this.taskmanager.start();
  }

  /**
   * stops the taskmanager
   */
  public async stopTaskmanager () {
    this.taskmanager.stop();
  }
}