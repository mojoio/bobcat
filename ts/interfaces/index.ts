export interface IMinerStatus {
  status: string;
  gap: string;
  miner_height: string;
  blockchain_height: string;
  epoch: 'rpc';
}

export interface IMinerDetailsResponse {
  ota_version: string;
  region: 'region_eu868';
  frequency_plan: 'eu868';
  animal: string;
  pubkey: string;
  miner: {
    State: 'running';
    Status: string;
    Names: ['/miner'];
    Image: string;
    Created: number;
  };
  p2p_status: string[];
  miner_height: string;
  epoch: string;
  ports_desc: "only need to port forward 44158. For 22, only when need remote support. public port open/close isn't accurate here, if your listen_addr is IP address, it should be OK";
  ports: { [key: string]: string };
  private_ip: string;
  public_ip: string;
  peerbook: string[];
  height: string[];
  temp0: string;
  temp1: string;
  timestamp: string;
  errors: string;
}
